require_relative 'person'

class TypeValidation < Person
  validate :owner, type: Person
end
