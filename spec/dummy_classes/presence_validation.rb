require_relative 'person'

class PresenceValidation < Person
  validate :name, presence: true
end
