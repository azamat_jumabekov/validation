require_relative 'person'

class ComplexValidation < Person
  validate :email, format: /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/
  validate :name, presence: true
  validate :owner, type: Person
end
