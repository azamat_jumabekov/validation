require 'validation'

class Person
  include Validation

  attr_accessor :name, :number, :owner, :email

  def initialize(name: nil, number: nil, owner: nil, email: nil)
    self.name = name
    self.number = number
    self.owner = owner
    self.email = email
  end
end
