require_relative '../dummy_classes/complex_validation'
require_relative '../dummy_classes/person'

RSpec.describe ComplexValidation do
  subject(:person) { described_class.new(**params) }
  let(:params) { { email: email, owner: owner, name: name } }
  let(:email) { 'user@example.com' }
  let(:owner) { Person }
  let(:name) { 'John' }

  describe 'succeeds' do
    it 'with valid params' do
      expect(subject.valid?).to be_truthy
    end
  end

  describe 'fails with' do
    describe 'no params' do
      let(:email) { nil }
      let(:owner) { nil }
      let(:name) { nil }

      it 'all validations' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors.base).to include({:email=>"Incorrect format"})
        expect(subject.errors.base).to include({:name=>"Can not be empty"})
        expect(subject.errors.base).to include({:owner=>"Icorrect class type"})
      end
    end

    describe 'wrong email' do
      let(:email) { 'wrongemail' }

      it 'email format validation' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors.base).to eq([{:email=>"Incorrect format"}])
      end
    end

    describe 'name missing' do
      let(:name) { nil }

      it 'name presence validation' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors.base).to eq([{:name=>"Can not be empty"}])
      end
    end

    describe 'wrong owner type' do
      let(:owner) { class InvalidClass; end }

      it 'owner type validation' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors.base).to eq([{:owner=>"Icorrect class type"}])
      end
    end
  end
end
