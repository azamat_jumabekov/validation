require_relative '../dummy_classes/presence_validation'

RSpec.describe PresenceValidation do
  subject(:person) { described_class.new(**params) }

  describe 'succeeds' do
    let(:params) { { name: 'John Doe' } }

    it 'with valid params' do
      expect(subject.valid?).to be_truthy
    end
  end

  describe 'fails' do
    let(:params) { {} }

    it 'with name missing error' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors.base).to include({:name=>"Can not be empty"})
    end

    it 'raises validation error if invalid' do
      expect{ subject.validate! }.to raise_error(Validation::ValidationFailed)
    end
  end
end
