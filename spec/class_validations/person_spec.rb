require_relative '../dummy_classes/person'

RSpec.describe Person do
  subject(:person) { described_class.new }

  it 'validates person instance' do
    expect(subject.validate!).to be_truthy
  end

  it 'checks if instance is valid' do
    expect(subject.valid?).to be_truthy
  end

  it 'has errors accessor' do
    expect(subject.respond_to?(:errors)).to be_truthy
  end
end
