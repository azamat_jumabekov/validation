require_relative '../dummy_classes/type_validation'
require_relative '../dummy_classes/person'

RSpec.describe TypeValidation do
  subject(:person) { described_class.new(**params) }

  describe 'succeeds' do
    let(:params) { { owner: Person } }

    it 'with valid params' do
      expect(subject.valid?).to be_truthy
    end
  end

  describe 'fails' do
    let!(:invalid_class) { class InvalidClass; end }
    let(:params) { { owner: InvalidClass } }

    it 'raises validation error if invalid' do
      expect{ subject.validate! }.to raise_error(Validation::ValidationFailed)
    end
  end
end
