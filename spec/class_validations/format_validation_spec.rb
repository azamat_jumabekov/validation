require_relative '../dummy_classes/format_validation'

RSpec.describe FormatValidation do
  subject(:person) { described_class.new(**params) }

  describe 'succeeds' do
    let(:params) { { email: 'user@example.com' } }

    it 'with valid params' do
      expect(subject.valid?).to be_truthy
    end
  end

  describe 'fails' do
    let(:params) { { email: 'a#&^w@.com' } }

    it 'with wrong format' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors.base).to include({:email=>"Incorrect format"})
    end

    it 'raises validation error if invalid' do
      expect{ subject.validate! }.to raise_error(Validation::ValidationFailed)
    end
  end
end
