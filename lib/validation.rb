require_relative 'validation/runner'
require_relative 'validation/errors'

module Validation
  def self.included(klass)
    klass.extend(ClassMethods)
  end

  def validate!
    validate
    raise ValidationFailed, self.errors.to_s if invalid?
    self.errors.empty?
  end

  def validate
    run_validations
  end

  def valid?
    validate
    self.errors.empty?
  end

  def invalid?
    !valid?
  end

  def errors
    @errors ||= Validation::Errors.new
  end

  def run_validations
    errors.clear
    Validation::Runner.new(self, self.class._validations).run
  end

  module ClassMethods
    attr_accessor :_validations

    def validate(attribute, options)
      self._validations ||= []
      self._validations << [attribute, options]
    end

    def validations
      self._validations
    end
  end

  class ValidationFailed < StandardError
  end
end
