module Validation
  module Validators
    class Base
      def initialize(klass, attribute, **args)
        @klass = klass
        @attribute = attribute
        @args = args
      end

      private

      def attribute_value
        @klass.instance_variable_get("@#{@attribute}")
      end
    end
  end
end
