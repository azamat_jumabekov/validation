require_relative 'base'

module Validation
  module Validators
    class Format < Base
      def run_validation
        return if @args[:format].match?(attribute_value.to_s)

        @klass.errors.add Hash[@attribute, 'Incorrect format']
      end
    end
  end
end
