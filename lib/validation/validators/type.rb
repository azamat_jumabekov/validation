require_relative 'base'

module Validation
  module Validators
    class Type < Base
      def run_validation
        @klass.errors.add Hash[@attribute, 'Icorrect class type'] unless (attribute_value == @args[:type])
      end
    end
  end
end
