require_relative 'base'

module Validation
  module Validators
    class Presence < Base
      def run_validation
        @klass.errors.add Hash[@attribute, 'Can not be empty'] if attribute_value.nil?
      end
    end
  end
end
