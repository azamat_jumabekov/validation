require_relative './validators/presence'
require_relative './validators/format'
require_relative './validators/type'

module Validation
  class Runner
    def initialize(klass, validation_rules)
      @klass = klass
      @rules = validation_rules
    end

    def run
      (@rules || []).each do |attribute, option|
        type, parameter = option.to_a.flatten
        validator_class = available_validators.fetch(type)
        validator_class.new(@klass, attribute, **option).run_validation
      end
    end

    def available_validators
      {
        presence: Validation::Validators::Presence,
        format: Validation::Validators::Format,
        type: Validation::Validators::Type
      }
    end
  end
end
