module Validation
  class Errors
    attr_reader :base

    def initialize
      @base = []
    end

    def add(error)
      @base << error
    end

    def present?
      !empty?
    end

    def empty?
      @base.empty?
    end

    def clear
      @base = []
    end

    def to_s
      "ERRORS - " + base.to_s
    end
  end
end
